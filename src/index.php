<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Teste Btime</title>
        <!-- Bootstrap -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/style.css" rel="stylesheet">

    </head>
    <body>
        <div class="container container-fluid topmargin-sm">
            <div class="jumbotron">
                <h1>Teste Btime</h1>
                <h3>Escrevendo no Celular <span class="lead"><a href="http://dojopuzzles.com/problemas/exibe/escrevendo-no-celular/">http://dojopuzzles.com/problemas/exibe/escrevendo-no-celular/</a></span></h3>

                <p>
                    Um dos serviços mais utilizados pelos usuários de aparelhos celulares são os SMS (Short Message Service), que permite o envio de mensagens curtas (até 255 caracteres em redes GSM e 160 caracteres em redes CDMA).
                </p>
                <p>  Para digitar uma mensagem em um aparelho que não possui um teclado QWERTY embutido é necessário fazer algumas combinações das 10 teclas numéricas do aparelho para conseguir digitar. Cada número é associado a um conjunto de letras como a seguir:
                </p>
                <table class="table table-striped table-hover table-bordered text-center">
                    <thead>
                        <tr>
                            <td>Letras</td>
                            <td> Números</td>
                        </tr>

                    </thead>
                    <tbody>
                        <tr>
                            <td>ABC</td>
                            <td>2</td>
                        </tr>
                        <tr>
                            <td>DEF</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>GHI</td>
                            <td>4</td>
                        </tr>
                        <tr>
                            <td>JKL</td>
                            <td>5</td>
                        </tr>
                        <tr>
                            <td>MNO</td>
                            <td>6</td>
                        </tr>
                        <tr>
                            <td>PQRS</td>
                            <td>7</td>
                        </tr>
                        <tr>
                            <td>TUV</td>
                            <td>8</td>
                        </tr>
                        <tr>
                            <td>WXYZ</td>
                            <td>9</td>
                        </tr>
                        <tr>
                            <td>Espaço</td>
                            <td>0</td>
                        </tr>
                    </tbody>

                </table>

                <p> Desenvolva um programa que, dada uma mensagem de texto limitada a 255 caracteres,
                    retorne a seqüência de números que precisa ser digitada.
                    Uma pausa, para ser possível obter duas letras referenciadas pelo mesmo número, 
                    deve ser indicada como _.</p>

                <p>Por exemplo, para digitar "SEMPRE ACESSO O DOJOPUZZLES", você precisa digitar:</p>
                <p> <strong>77773367_7773302_222337777_777766606660366656667889999_9999555337777</strong></p>
            </div>
            <form id="form_1" class="form" name="form_1" method="POST" action="">
                <div class="row">
                    <div class="col-sm-5">

                        <div class="panel panel-info">
                            <div class="panel-heading">Entre com a mensagem</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label>Mensagem:</label>
                                    <textarea placeholder="Digite sua mensagem e clique no botão ao lado"  id="msg" name="msg" maxlength="255" class="form-control" aria-describedby="helpmsg" rows="4"></textarea>
                                    <span id="helpmsg" class="help-block">Máx. 255 carácteres</span>

                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="col-sm-1 text-center ">
                        <button id="btn_converter" title="Converter"  class="btn btn-success btn-lg topmargin-lg bottommargin-sm">
                            <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span>
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <div class="panel panel-info">
                            <!-- Default panel contents -->
                            <div class="panel-heading">Sequência de números necessários para formar a mensagem: </div>
                            <div class="panel-body">
                                <p class="msg-convertida">
                                    


                                </p>
                            </div>


                        </div>

                    </div>
                </div>
            </form>


            <footer class="footer">
                <p class="text-center">&copy; 2017 Josiano Carvalho.</p>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                <script src="assets/bootstrap/js/bootstrap.min.js"></script>
                <script src="assets/js/init.js"></script>
            </footer>
        </div> <!-- /container -->
    </body>
</html>

