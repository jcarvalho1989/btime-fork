$(document).ready(function () {

    $('#btn_converter').on('click', function (event) {
        var $area_msg_convertida = $('.msg-convertida');
        var btn_action = $(this);
        var $form = $('#form_1');
        var $data = $form.serialize();
        btn_action.addClass('disabled');

        $.ajax({
            url: 'include/mensagem_convertida_ajax.php',
            data: $data,
            type: "POST",
            success: function (result) {
                $area_msg_convertida.html(result.msg_convertida);
                btn_action.removeClass('disabled');
            }

        });

        event.preventDefault();
        event.stopPropagation();

    });
});

