<?php

require_once '../vendor/autoload.php';
header('Content-Type: application/json');

use Btime\TestDojo1\Sms as Sms;

if (isset($_POST) && !empty($_POST['msg'])) {
    $sms = new Sms();
    $mensagem = $_POST['msg'];
    $sms->mensagemEntrada($mensagem, 255);
    $sms_convertido = $sms->getSequenciaNumerica();
    $erro = $sms->getMsgErro();
    $retorno['msg_convertida'] = $sms_convertido;
    $retorno['erro'] = $erro;

    echo json_encode($retorno);
}
?>