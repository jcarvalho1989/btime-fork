<?php

/* Problema:
 * http://dojopuzzles.com/problemas/exibe/escrevendo-no-celular/

  Desenvolva um programa que, dada uma mensagem de texto limitada a 255 caracteres,
  retorne a seqüência de números que precisa ser digitada.
  Uma pausa, para ser possível obter duas letras referenciadas pelo mesmo número, deve ser indicada como _.
  Por exemplo, para digitar "SEMPRE ACESSO O DOJOPUZZLES", você precisa digitar:
  77773367_7773302_222337777_777766606660366656667889999_9999555337777
 */

namespace Btime\TestDojo1;

class Sms
{

    private $sequencia_numerica;
    private $msg_erro;

    public function mensagemEntrada($mensagem, $total_caracteres = 255)
    {
        if (strlen($mensagem) <= $total_caracteres) {
            $mensagem = strtoupper($this->removeLetrasAcentuadas($mensagem));
            $mensagem_to_array = str_split($mensagem);

            $msg_convertida = "";
            foreach ($mensagem_to_array as $letra) {
                $prox_seq = $this->converteLetraDigitada($letra);
                $pausa = $this->geraPausa($msg_convertida, $prox_seq);
                $msg_convertida .= $pausa . $prox_seq;
            }
            $this->setSequenciaNumerica($msg_convertida);
        } else {
            $this->setMsgerro("Mensagem acima de 255 caracteres!");
        }
    }

    public function geraPausa($seq_anterior, $prox_seq, $separador = "_")
    {
        if (substr($seq_anterior, -1) == $prox_seq[0]) {
            return $separador;
        }
    }

    public function combinacoes()
    {
        return $combinacoes = array(
            0 => " ",
            2 => 'ABC',
            3 => "DEF",
            4 => "GHI",
            5 => "JKL",
            6 => "MNO",
            7 => "PQRS",
            8 => "TUV",
            9 => "WXYZ"
        );
    }

    public function converteLetraDigitada($letra)
    {
        $seq_digitada = "";
        $combinacoes = $this->combinacoes();
        foreach ($combinacoes as $tecla => $conj_letras) {
            $posicao = strpos($conj_letras, $letra);
            if ($posicao !== false) {
                $posicao + 1;
                for ($i = 0; $i <= ($posicao); $i++) {
                    $seq_digitada .= $tecla;
                }
            }
        }
        return $seq_digitada;
    }

    function removeLetrasAcentuadas($mensagem)
    {
        return preg_replace(array(
            "/(ç)/", "/(á|à|ã|â|ä)/",
            "/(Á|À|Ã|Â|Ä)/",
            "/(é|è|ê|ë)/",
            "/(É|È|Ê|Ë)/",
            "/(í|ì|î|ï)/",
            "/(Í|Ì|Î|Ï)/",
            "/(ó|ò|õ|ô|ö)/",
            "/(Ó|Ò|Õ|Ô|Ö)/",
            "/(ú|ù|û|ü)/",
            "/(Ú|Ù|Û|Ü)/",
            "/(ñ)/",
            "/(Ñ)/"), explode(" ", "c a A e E i I o O u U n N"), $mensagem);
    }

    //getters & setters
    function getSequenciaNumerica()
    {
        return $this->sequencia_numerica;
    }

    function getMsgErro()
    {
        return $this->msg_erro;
    }

    function setSequenciaNumerica($sequencia_numerica)
    {
        $this->sequencia_numerica = $sequencia_numerica;
    }

    function setMsgErro($msg_erro)
    {
        $this->msg_erro = $msg_erro;
    }

}
